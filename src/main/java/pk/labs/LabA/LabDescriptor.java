package pk.labs.LabA;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabA.Impl.DisplayImpl";
    public static String controlPanelImplClassName = "pk.labs.LabA.Impl.ControlPanelImpl";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.Main";
    public static String mainComponentImplClassName = "pk.labs.LabA.Impl.MainImpl";
    // endregion

    // region P2
    public static String mainComponentBeanName = "main";
    public static String mainFrameBeanName = "main-frame";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "silly-frame";
    // endregion
}
