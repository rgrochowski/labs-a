/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabA.Impl;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.GridLayout;
import pk.labs.LabA.Contracts.Display;

public class DisplayImpl extends JPanel implements Display
{
  private JLabel paramLabel;
  
  public DisplayImpl()
  {
      super();
      this.paramLabel = new JLabel();
      this.paramLabel.setText("Test");
      this.add(this.paramLabel, 0);
      initComponents();
  }
  private void initComponents()
  {
    this.setLayout(new GridLayout(1,1));
  }
  
  @Override
  public JPanel getPanel()
  {
    return this;
  }
  
  @Override
  public void setText(String text)
  {
    ((JLabel)this.getComponent(0)).setText(text);
  }
  
  public JLabel getLabel()
  {
    return (JLabel)this.getComponent(0);
  }
  
  public void setLabel(JLabel label)
  {
    this.paramLabel = label;
  }
}