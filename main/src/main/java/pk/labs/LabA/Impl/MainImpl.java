/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabA.Impl;
import pk.labs.LabA.Contracts.Main;
import pk.labs.LabA.Contracts.Display;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

public class MainImpl implements Main {
    
  private Display screen;
  public MainImpl(){
      
  }
  
  public MainImpl(Display screen)
  {
    this.screen = screen;
  }
  
  public Display getScreen()
  {
    return this.screen;
  }
  
  public void setScreen(Display screen)
  {
    this.screen = screen;
  }
  
  public void JedzDoGory()
  {
    getScreen().setText("Jade do góry...");
  }
  
  public void JedzNaDol()
  {
    getScreen().setText("Jade w dół...");
  }
    
}
