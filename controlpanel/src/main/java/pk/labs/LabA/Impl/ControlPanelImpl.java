/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabA.Impl;
import pk.labs.LabA.Contracts.ControlPanel;
import pk.labs.LabA.Contracts.Main;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

public class ControlPanelImpl extends JPanel implements ControlPanel {
 private Main main;
 private JButton jButton1;
 private JButton jButton2;
  
 private void initComponents()
  {
    this.jButton1 = new JButton();
    this.jButton2 = new JButton();
    this.jButton1.setText("Gora");
    this.jButton2.setText("Dol");
    this.jButton1.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent evt)
      {
        ControlPanelImpl.this.jButton1ActionPerformed(evt);
      }
    });
    this.jButton2.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent evt)
      {
        ControlPanelImpl.this.jButton2ActionPerformed(evt);
      }
    });
    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jButton2, -1, -1, 32767).addComponent(this.jButton1, -1, -1, 32767).addContainerGap(-1, 32767)));
    layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jButton1, -2, 58, -2).addGap(18, 18, 18).addComponent(this.jButton2).addContainerGap(-1, 32767)));
  }
  private void jButton1ActionPerformed(ActionEvent evt)
  {
    this.main.JedzDoGory();
  }
  
  private void jButton2ActionPerformed(ActionEvent evt)
  {
    this.main.JedzNaDol();
  }

  public ControlPanelImpl(){
      
  }
  public ControlPanelImpl(Main main)
  {
    this.main = main;
    initComponents();
  }
  
  public Main getMain()
  {
    return this.main;
  }
  
  public void setMain(Main main)
  {
    this.main = main;
  }
  
  public JPanel getPanel()
  {
    return this;
  }   
}
