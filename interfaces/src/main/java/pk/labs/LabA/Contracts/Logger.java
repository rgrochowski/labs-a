package pk.labs.LabA.Contracts;

public abstract interface Logger 
{

   public void logMethodEntrance(String string, Object[] os);

   public void logMethodExit(String string, Object o);
}