package pk.labs.LabA.Contracts;

import javax.swing.JPanel;

public abstract interface Control
{
  public abstract JPanel getPanel();
}